-- A (P12M):
select distinct csn from link.view_facttrans_plus
where corporation_id in ('FP','101770000000','100020000000','101780000000')
and pool_id='P01' and club_code='LNK' and cancel_ind='N' and pointid='I' and transaction_date>=dateadd(month, -12, getdate()) and est_gross_transaction_amt>0
and csn not in
(select distinct csn from link.view_facttrans_plus
where corporation_id not in ('FP','101770000000','100020000000','101780000000','OCBC')
and pool_id='P01' and club_code='LNK' and cancel_ind='N' and pointid='I' and transaction_date>=dateadd(month, -12, getdate()) and est_gross_transaction_amt>0);

-- B (P12M):
select distinct csn from link.view_facttrans_plus
where corporation_id in ('FP','101770000000','100020000000','101780000000')
and pool_id='P01' and club_code='LNK' and cancel_ind='N' and pointid='I' and transaction_date>=dateadd(month, -12, getdate()) and est_gross_transaction_amt>0
and csn in
(select distinct csn from link.view_facttrans_plus
where corporation_id not in ('FP','101770000000','100020000000','101780000000', 'OCBC')
and pool_id='P01' and club_code='LNK' and cancel_ind='N' and pointid='I' and transaction_date>=dateadd(month, -12, getdate()) and est_gross_transaction_amt>0);

-- C (P12M):
select distinct csn from link.view_facttrans_plus
where corporation_id not in ('FP','101770000000','100020000000','101780000000')
and pool_id='P01' and club_code='LNK' and cancel_ind='N' and pointid='I' and transaction_date>=dateadd(month, -12, getdate()) and est_gross_transaction_amt>0
and csn not in
(select distinct csn from link.view_facttrans_plus
where corporation_id in ('FP','101770000000','100020000000','101780000000')
and pool_id='P01' and club_code='LNK' and cancel_ind='N' and pointid='I' and transaction_date>=dateadd(month, -12, getdate()) and est_gross_transaction_amt>0);