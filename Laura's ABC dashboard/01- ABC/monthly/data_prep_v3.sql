create temp table inside_trx as(
  select * from lh_dm_trx2 where 
  transaction_date>= trunc(add_months(current_date_parameter , -1))    and
  transaction_date< current_date_parameter  and outside_merchant=0);

create temp table outside_trx as(
  select * from lh_dm_trx2 where
  transaction_date>= trunc(add_months(current_date_parameter , -1))    and
  transaction_date< current_date_parameter  and outside_merchant=1);

---- inside
create temp table inside_metrics as(
  select csn, sum(spending) as inside_spend, count(*) as inside_volume from inside_trx
  group by csn
);


---- outside
create temp table outside_spend as
(
  select csn, sum(spending) as outside_spend 
  from outside_trx
  group by csn
);


create temp table outside_volume as
(
  select csn, 
  count(*) as outside_volume, 
  count(distinct corp_group) as no_of_outside_merchants_visits,
  max(transaction_date) as last_outside_purchase_date
  from  outside_trx
  group by csn
);


create temp table outside_metrics as select
a.csn, 
b.outside_spend, 
a.outside_volume,
a.no_of_outside_merchants_visits,
a.last_outside_purchase_date  
from outside_volume as a left join  outside_spend as b on a.csn=b.csn;

----- 4> final abc table on csn level----- 
--  drop table if exists abc_csn_level;
create temp table  abc_csn_level as
select *,
case 
when inside_volume is not null and outside_volume is null then 'A'
when inside_volume is not null and outside_volume is not null  then 'B'
when inside_volume is null and outside_volume is not null  then 'C'
end ABC_group,
case
when outside_spend>=100   then 1
when outside_spend<100 then  0
end  B_bigger_than_100  
from 
(
  select distinct 
  nvl(a.csn, b.csn ) as csn,
  a.inside_spend,
  a.inside_volume,
  b.outside_spend,
  b.outside_volume,
  b.no_of_outside_merchants_visits,
  b.last_outside_purchase_date,
  getdate() as update_date,
  '1month' as analysis_period
  
  from  inside_metrics	as a 
  full outer join 
  outside_metrics as b
  on a.csn=b.csn
);



------------------------------------------------------------------
  --- dashboard
------------------------------------------------------------------
  --- dashboard
--drop table if exists dashboard;
create temp table dashboard as 
(
  select
  update_date,
  trunc(dateadd(day, -1,current_date_parameter))as month_year,
  abc_group,B_bigger_than_100,
  count(distinct csn) as csn_count,
  sum(inside_spend)as inside_spend,
  sum(inside_volume)as inside_volume,
  sum(outside_spend)as outside_spend,
  sum(outside_volume)as outside_volume,
  sum(no_of_outside_merchants_visits) as no_of_outside_merchants_visits
  from abc_csn_level group by update_date,month_year,abc_group,B_bigger_than_100
);

delete from abc_dashboard_v2
where extract(month from month_year) =extract(month from trunc(dateadd(day, -1,current_date_parameter))) and
extract(year from month_year) =extract(year from trunc(dateadd(day, -1,current_date_parameter))) 
;

insert into abc_dashboard_v2
(select * from dashboard);
