

DROP TABLE IF EXISTS abc_dashboard_v2 CASCADE;
CREATE TABLE abc_dashboard_v2
(
  update_date        date,
  month_year         date,
  abc_group          varchar(1),
  b_bigger_than_100  integer,
  csn_count          bigint,
  inside_spend       numeric(38,4),
  inside_volume      bigint,
  outside_spend      numeric(38,4),
  outside_volume     bigint,
  no_of_outside_merchants_visits     bigint
);



