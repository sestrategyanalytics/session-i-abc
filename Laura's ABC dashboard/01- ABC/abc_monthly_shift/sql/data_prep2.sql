
			create temp table window_n as
			select * from lh_dm_trx2 where  transaction_date<='2019-01-01' and 
			extract(month from transaction_date) =extract(month from  trunc(DATEADD(day,  -1,'2019-01-01'))) 
			and extract(year from transaction_date) =extract(year from  trunc(DATEADD(day,  -1,'2019-01-01')));
			

			create temp table window_n_1 as
			select * from lh_dm_trx2 where  
			extract(month from transaction_date) =extract(month from  trunc(DATEADD(day,  -1,'2019-01-01')))-1 
			and extract(year from transaction_date) =extract(year from  trunc(DATEADD(day,  -1,'2019-01-01')))
			;


      create temp table label_n as
           select csn,
                    case when count(distinct outside_merchant)=2 then 2
                    when  count(distinct outside_merchant)=1 and max(outside_merchant)=0 then 1
                    when  count(distinct outside_merchant)=1 and max(outside_merchant)=1 then 3
                    end abc_n
                    from window_n
                    group by csn;

      create temp table label_n_1 as
           select csn,
                    case when count(distinct outside_merchant)=2 then 2
                    when  count(distinct outside_merchant)=1 and max(outside_merchant)=0 then 1
                    when  count(distinct outside_merchant)=1 and max(outside_merchant)=1 then 3
                    end abc_n_1
                    from window_n_1
                    group by csn;
                    

     create temp table final as
     select nvl(a.csn,b.csn) as csn,nvl(a.abc_n,0) as abc_n,nvl(b.abc_n_1,0) as abc_n_1, 
     trunc(dateadd(day, -1,'2019-01-01'))as month_year,getdate() as update_date 
     from label_n as a full outer join label_n_1 as b on a.csn=b.csn;
     


      delete from lh_abc_dashboard_monthly_shift
      where 
      extract(month from month_year) =extract(month from trunc(dateadd(day, -1,'2019-01-01'))) and
      extract(year from  month_year) =extract(year from trunc(dateadd(day,  -1,'2019-01-01')))  and
      extract(day from   month_year) =extract(day from trunc(dateadd(day,  -1,'2019-01-01')))  
      ;
      
insert into lh_abc_dashboard_monthly_shift (select * from final);
 
