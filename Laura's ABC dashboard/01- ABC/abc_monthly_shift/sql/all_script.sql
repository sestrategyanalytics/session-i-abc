DROP TABLE IF EXISTS lh_abc_dashboard_monthly_shift CASCADE;

CREATE TABLE lh_abc_dashboard_monthly_shift
(
   csn          varchar(20),
   abc_n        integer,
   abc_n_1      integer,
   month_year   date,
   update_date  timestamp
);
