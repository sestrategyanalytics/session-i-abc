	--drop table if exists training_window;
			create temp table training_window as
			select * from lh_dm_trx2 where  transaction_date<= trunc(DATEADD(day,  -1,'2019-01-01'))
			and transaction_date>='2018-01-01'
			;
			

      --drop table if exists o;
      create temp table o as 
      select csn,
      case when count(distinct outside_merchant)=2 then 2
      when  count(distinct outside_merchant)=1 and max(outside_merchant)=0 then 1
      when  count(distinct outside_merchant)=1 and max(outside_merchant)=1 then 3
      end ABC_type_n
      from training_window 	
      group by csn;
      

     -- drop table if exists B_spending;
     -- create temp table B_spending as
     -- select csn,corp_group,branch_name_english_1,sum(spending),count(*) from training_window where outside_merchant=1 and 
     -- csn in (select csn from o where ABC_type_n>1)
     -- group by csn,corp_group,branch_name_english_1;
      
      
      --drop table if exists A_to_B_csn2;
      --create temp table A_to_B_csn2 as
      --select a.*, b.edm_base,b.sms_base,c.branch_name_english_1, c.corp_group,c.sum as spending,c.count as count,
     -- trunc(dateadd(day, -0,'2019-01-01'))as month_year,
      --getdate() as update_date
      --from o as a left join lh_dm_csn as b on a.csn=b.csn
      --left join B_spending as c on a.csn=c.csn;
      
     --drop table if exists A_to_B_csn2;
      create temp table A_to_B_csn2 as
      select a.*, b.edm_base,b.sms_base,
      trunc(dateadd(day, -1,'2019-01-01'))as month_year,
      getdate() as update_date
      from o as a left join lh_dm_csn as b on a.csn=b.csn;
      
--select * from B_spending where branch_name_english_1 is null
--select top 10 * from A_to_B_csn2
-------dashboard 

delete from lh_abc_dashboard_accumulation
where 
extract(month from month_year) =extract(month from trunc(dateadd(day, -1,'2019-01-01'))) and
extract(year from  month_year) =extract(year from trunc(dateadd(day,  -1,'2019-01-01')))  and
extract(day from   month_year) =extract(day from trunc(dateadd(day,  -1,'2019-01-01')))  
;


insert into lh_abc_dashboard_accumulation
     (select * from A_to_B_csn2);
     

--select * from lh_dm_trx2 where csn ='2467579'

--drop table if exists lh_test;
--create table lh_test as select top 10* from A_to_B_csn2


--select top 10 * from A_to_B_csn2

--select distinct month_year from lh_abc_dashboard_accumulation
