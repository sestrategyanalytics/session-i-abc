DROP TABLE IF EXISTS lh_abc_dashboard_accumulation CASCADE;

CREATE TABLE lh_abc_dashboard_accumulation
(
   csn          varchar(20),
   abc_type_n   integer,
   edm_base     varchar(8),
   sms_base     varchar(8),
   month_year   date,
   update_date  timestamp
);
