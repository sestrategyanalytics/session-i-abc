
			--drop table if exists training_window;
			create temp table training_window as
			select * from lh_dm_trx2 where  transaction_date<= trunc(DATEADD(day,  -1,CURRENT_DATE))
			and transaction_date> trunc( dateadd(day,-1,add_months(CURRENT_DATE , -6)))
			;
			
			--drop table if exists label_window;
			create temp table label_window as
			select * from lh_dm_trx2 where  transaction_date>trunc(DATEADD(day,  -1,CURRENT_DATE))
			and transaction_date<= trunc(DATEADD(day,  -0,CURRENT_DATE));

      --drop table if exists o;
      create temp table o as 
      select csn,
      case when count(distinct outside_merchant)=2 then 2
      when  count(distinct outside_merchant)=1 and max(outside_merchant)=0 then 1
      when  count(distinct outside_merchant)=1 and max(outside_merchant)=1 then 3
      end ABC_type_n
      from training_window 	
      group by csn;
      
      --drop table if exists abc_labels;
      create temp table abc_labels as 
      select csn,
      case when count(distinct outside_merchant)=2 then 2
      when  count(distinct outside_merchant)=1 and max(outside_merchant)=0 then 1
      when  count(distinct outside_merchant)=1 and max(outside_merchant)=1 then 3
      end abc_labels
      from label_window 	
      group by csn;
      
      
      --drop table if exists  LH_next_purchse_new_abc_labels;
      create temp table LH_next_purchse_new_abc_labels as
      select distinct 
      a.csn,
      nvl(o.ABC_type_n,0) as abc_n,
      nvl(abc_labels.abc_labels,0) as abc_labels
      from client_segmentation_feature as a
      left join o on a.csn=o.csn
      left join abc_labels on a.csn=abc_labels.csn;
      
      --drop table if exists A_to_B_csn;
      create temp table A_to_B_csn as
      select * from LH_next_purchse_new_abc_labels where abc_n=1 and abc_labels=2;
      
      --drop table if exists B_spending;
      create temp table B_spending as
      select csn,corp_group,branch_name_english_1, sum(spending),count(*) from label_window where outside_merchant=1 and 
      csn in (select csn from A_to_B_csn)
      group by csn,corp_group,branch_name_english_1;
      
      
      --drop table if exists A_to_B_csn2;
      create temp table A_to_B_csn2 as
      select a.*, b.edm_base,b.sms_base,c.branch_name_english_1, c.corp_group,c.sum as spending,
      trunc(dateadd(day, -0,CURRENT_DATE))as month_year,
      getdate() as update_date
      from A_to_B_csn as a left join lh_dm_csn as b on a.csn=b.csn
      left join B_spending as c on a.csn=c.csn;
      
      
-------dashboard 

delete from lh_abc_dashboard_daily
where 
extract(month from month_year) =extract(month from trunc(dateadd(day, -0,CURRENT_DATE))) and
extract(year from month_year) =extract(year from trunc(dateadd(day, -0,CURRENT_DATE)))  and
extract(day from month_year) =extract(day from trunc(dateadd(day, -0,CURRENT_DATE)))  
;

insert into lh_abc_dashboard_daily
     (select * from A_to_B_csn2);

      

      
