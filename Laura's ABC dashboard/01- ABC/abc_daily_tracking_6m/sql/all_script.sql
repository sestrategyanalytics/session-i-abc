DROP TABLE IF EXISTS lh_abc_dashboard_daily CASCADE;

CREATE TABLE lh_abc_dashboard_daily
(
   csn                    varchar(20),
   abc_n                  integer,
   abc_labels             integer,
   edm_base               varchar(8),
   sms_base               varchar(8),
   branch_name_english_1  varchar(64),
   corp_group             varchar(64),
   spending               numeric(38,4),
   month_year             date,
   update_date            timestamp
);
