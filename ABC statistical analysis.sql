--------------------------------------------------------------------------------------- ABC 2018: a statistical analysis ---------------------------------------------------------------------------------------
SELECT yearid, monthid, csn
into #outside
FROM link.view_facttrans_plus
WHERE corporation_id NOT IN ('FP','101770000000','100020000000','101780000000','OCBC')
AND POOL_ID='P01' AND CLUB_CODE ='LNK' AND CANCEL_IND = 'N' and pointID = 'I' and yearid = 2018 and est_gross_transaction_amt>0
GROUP BY yearid, monthid, csn;

SELECT yearid, monthid, csn
into #inside
FROM link.view_facttrans_plus
WHERE corporation_id IN ('FP','101770000000','100020000000','101780000000','OCBC')
AND POOL_ID='P01' AND CLUB_CODE ='LNK' AND CANCEL_IND = 'N' and pointID = 'I' and yearid = 2018 and est_gross_transaction_amt>0
GROUP BY yearid, monthid, csn;

select a.yearid, a.monthid, a.csn
into #both
from #inside a
join #outside b
on a.yearid=b.yearid and a.monthid=b.monthid and a.csn=b.csn;

-- A in 2017, B in 2018
select count(distinct csn)
FROM link.view_facttrans_plus
WHERE corporation_id IN ('FP','101770000000','100020000000','101780000000','OCBC')
AND POOL_ID='P01' AND CLUB_CODE ='LNK' AND CANCEL_IND = 'N' and pointID = 'I' and yearid = 2017 and est_gross_transaction_amt>0
and csn not in (select csn from link.view_facttrans_plus
WHERE corporation_id not IN ('FP','101770000000','100020000000','101780000000','OCBC')
AND POOL_ID='P01' AND CLUB_CODE ='LNK' AND CANCEL_IND = 'N' and pointID = 'I' and yearid = 2017 and est_gross_transaction_amt>0)
and csn in (select csn from #inside)
and csn in (select csn from #outside);

-- B in 2017 and 2018
select count(distinct csn)
from link.view_facttrans_plus
  where csn in (select csn from #inside) and csn in (select csn from #outside)
  and csn in (select csn from link.view_facttrans_plus where corporation_id not in ('FP','101770000000','100020000000','101780000000','OCBC')
  and pool_id='P01' and club_code='LNK' and cancel_ind='N' and pointid='I' and est_gross_transaction_amt>0 and yearid=2017 and monthid::int<=12) 
  and csn in (select csn from link.view_facttrans_plus where corporation_id in ('FP','101770000000','100020000000','101780000000','OCBC')
  and pool_id='P01' and club_code='LNK' and cancel_ind='N' and pointid='I' and est_gross_transaction_amt>0 and yearid=2017 and monthid::int<=12);

-- C in 2017, B in 2018
select count(distinct csn)
FROM link.view_facttrans_plus
WHERE corporation_id  not IN ('FP','101770000000','100020000000','101780000000','OCBC')
AND POOL_ID='P01' AND CLUB_CODE ='LNK' AND CANCEL_IND = 'N' and pointID = 'I' and yearid = 2017 and est_gross_transaction_amt>0
and csn not in (select csn from link.view_facttrans_plus
WHERE corporation_id IN ('FP','101770000000','100020000000','101780000000','OCBC')
AND POOL_ID='P01' AND CLUB_CODE ='LNK' AND CANCEL_IND = 'N' and pointID = 'I' and yearid = 2017 and est_gross_transaction_amt>0)
and csn in (select csn from #inside)
and csn in (select csn from #outside);

-- x in 2017, B in 2018
select count(distinct csn)
FROM #inside
WHERE csn not in (select csn from link.view_facttrans_plus
WHERE POOL_ID='P01' AND CLUB_CODE ='LNK' AND CANCEL_IND = 'N' and pointID = 'I' and yearid = 2017 and est_gross_transaction_amt>0)
and csn in (select csn from #outside);

-- A in 2017 and 2018
select count(distinct csn)
FROM link.view_facttrans_plus
WHERE corporation_id IN ('FP','101770000000','100020000000','101780000000','OCBC')
AND POOL_ID='P01' AND CLUB_CODE ='LNK' AND CANCEL_IND = 'N' and pointID = 'I' and yearid = 2017 and est_gross_transaction_amt>0
and csn not in (select csn from link.view_facttrans_plus
WHERE corporation_id not IN ('FP','101770000000','100020000000','101780000000','OCBC')
AND POOL_ID='P01' AND CLUB_CODE ='LNK' AND CANCEL_IND = 'N' and pointID = 'I' and yearid = 2017 and est_gross_transaction_amt>0)
and csn in (select csn from #inside)
and csn not in (select csn from #outside);

-- B in 2017, A in 2018
select count(distinct csn)
from link.view_facttrans_plus
  where csn in (select csn from #inside) and csn not in (select csn from #outside)
  and csn in (select csn from link.view_facttrans_plus where corporation_id not in ('FP','101770000000','100020000000','101780000000','OCBC')
  and pool_id='P01' and club_code='LNK' and cancel_ind='N' and pointid='I' and est_gross_transaction_amt>0 and yearid=2017 and monthid::int<=12) 
  and csn in (select csn from link.view_facttrans_plus where corporation_id in ('FP','101770000000','100020000000','101780000000','OCBC')
  and pool_id='P01' and club_code='LNK' and cancel_ind='N' and pointid='I' and est_gross_transaction_amt>0 and yearid=2017 and monthid::int<=12);

-- Merchants
select a.corporation_id, corporate_name_english_1 as Merchant, count(distinct a.csn) as ActivatedCustomers from link.view_facttrans_plus a join #numberofmonths b
on a.csn=b.csn and a.monthid=b.Activation 
join link.link_cls_corporation c on a.corporation_id=c.corporate_id
where pool_id='P01' and club_code='LNK' and cancel_ind='N' and pointid='I' and est_gross_transaction_amt>0
and corporation_id not in ('FP','101770000000','100020000000','101780000000','OCBC')
and b.Activation::int>06
group by a.corporation_id, Merchant
order by ActivatedCustomers desc;

select a.corporation_id, corporate_name_english_1 as Merchant, count(distinct a.csn) as ActivatedCustomers from link.view_facttrans_plus a join #numberofmonths b
on a.csn=b.csn and a.monthid=b.Activation 
join link.link_cls_corporation c on a.corporation_id=c.corporate_id
where pool_id='P01' and club_code='LNK' and cancel_ind='N' and pointid='I' and est_gross_transaction_amt>0
and corporation_id not in ('FP','101770000000','100020000000','101780000000','OCBC')
and b.Activation::int<=06
group by a.corporation_id, Merchant
order by ActivatedCustomers desc;


select a.corporation_id, corporate_name_english_1 as Merchant, count(distinct a.csn) as DisengagedCustomers from link.view_facttrans_plus a 
join link.link_cls_corporation b on a.corporation_id=b.corporate_id
  where corporation_id not in ('FP','101770000000','100020000000','101780000000','OCBC')
  and pool_id='P01' and club_code='LNK' and cancel_ind='N' and pointid='I' and est_gross_transaction_amt>0 and yearid=2017 
  and csn in (select csn from #inside) and csn not in (select csn from #outside)
  and csn in (select csn from link.view_facttrans_plus where corporation_id not in ('FP','101770000000','100020000000','101780000000','OCBC')
  and pool_id='P01' and club_code='LNK' and cancel_ind='N' and pointid='I' and est_gross_transaction_amt>0 and yearid=2017 and monthid::int<=12) 
  and csn in (select csn from link.view_facttrans_plus where corporation_id in ('FP','101770000000','100020000000','101780000000','OCBC')
  and pool_id='P01' and club_code='LNK' and cancel_ind='N' and pointid='I' and est_gross_transaction_amt>0 and yearid=2017 and monthid::int<=12)
group by a.corporation_id, Merchant
order by DisengagedCustomers desc;

-- C in 2017, A in 2018
select count(distinct csn)
FROM link.view_facttrans_plus
WHERE corporation_id  not IN ('FP','101770000000','100020000000','101780000000','OCBC')
AND POOL_ID='P01' AND CLUB_CODE ='LNK' AND CANCEL_IND = 'N' and pointID = 'I' and yearid = 2017 and est_gross_transaction_amt>0
and csn not in (select csn from link.view_facttrans_plus
WHERE corporation_id IN ('FP','101770000000','100020000000','101780000000','OCBC')
AND POOL_ID='P01' AND CLUB_CODE ='LNK' AND CANCEL_IND = 'N' and pointID = 'I' and yearid = 2017 and est_gross_transaction_amt>0)
and csn in (select csn from #inside)
and csn not in (select csn from #outside);

-- x in 2017, A in 2018
select count(distinct csn)
FROM #inside
WHERE csn not in (select csn from link.view_facttrans_plus
WHERE POOL_ID='P01' AND CLUB_CODE ='LNK' AND CANCEL_IND = 'N' and pointID = 'I' and yearid = 2017 and est_gross_transaction_amt>0)
and csn not in (select csn from #outside);

-- A in 2017, C in 2018
select count(distinct csn)
FROM link.view_facttrans_plus
WHERE corporation_id IN ('FP','101770000000','100020000000','101780000000','OCBC')
AND POOL_ID='P01' AND CLUB_CODE ='LNK' AND CANCEL_IND = 'N' and pointID = 'I' and yearid = 2017 and est_gross_transaction_amt>0
and csn not in (select csn from link.view_facttrans_plus
WHERE corporation_id not IN ('FP','101770000000','100020000000','101780000000','OCBC')
AND POOL_ID='P01' AND CLUB_CODE ='LNK' AND CANCEL_IND = 'N' and pointID = 'I' and yearid = 2017 and est_gross_transaction_amt>0)
and csn not in (select csn from #inside)
and csn in (select csn from #outside);

-- B in 2017, C in 2018
select count(distinct csn)
from link.view_facttrans_plus
  where csn not in (select csn from #inside) and csn in (select csn from #outside)
  and csn in (select csn from link.view_facttrans_plus where corporation_id not in ('FP','101770000000','100020000000','101780000000','OCBC')
  and pool_id='P01' and club_code='LNK' and cancel_ind='N' and pointid='I' and est_gross_transaction_amt>0 and yearid=2017 and monthid::int<=12) 
  and csn in (select csn from link.view_facttrans_plus where corporation_id in ('FP','101770000000','100020000000','101780000000','OCBC')
  and pool_id='P01' and club_code='LNK' and cancel_ind='N' and pointid='I' and est_gross_transaction_amt>0 and yearid=2017 and monthid::int<=12);

-- C in 2017 and 2018
select count(distinct csn)
FROM link.view_facttrans_plus
WHERE corporation_id  not IN ('FP','101770000000','100020000000','101780000000','OCBC')
AND POOL_ID='P01' AND CLUB_CODE ='LNK' AND CANCEL_IND = 'N' and pointID = 'I' and yearid = 2017 and est_gross_transaction_amt>0
and csn not in (select csn from link.view_facttrans_plus
WHERE corporation_id IN ('FP','101770000000','100020000000','101780000000','OCBC')
AND POOL_ID='P01' AND CLUB_CODE ='LNK' AND CANCEL_IND = 'N' and pointID = 'I' and yearid = 2017 and est_gross_transaction_amt>0)
and csn not in (select csn from #inside)
and csn in (select csn from #outside);

-- x in 2017, C in 2018
select count(distinct csn)
FROM #outside
WHERE csn not in (select csn from link.view_facttrans_plus
WHERE POOL_ID='P01' AND CLUB_CODE ='LNK' AND CANCEL_IND = 'N' and pointID = 'I' and yearid = 2017 and est_gross_transaction_amt>0)
and csn not in (select csn from #inside);

-- A in 2017, x in 2018
select count(distinct csn)
FROM link.view_facttrans_plus
WHERE corporation_id IN ('FP','101770000000','100020000000','101780000000','OCBC')
AND POOL_ID='P01' AND CLUB_CODE ='LNK' AND CANCEL_IND = 'N' and pointID = 'I' and yearid = 2017 and est_gross_transaction_amt>0
and csn not in (select csn from link.view_facttrans_plus
WHERE corporation_id not IN ('FP','101770000000','100020000000','101780000000','OCBC')
AND POOL_ID='P01' AND CLUB_CODE ='LNK' AND CANCEL_IND = 'N' and pointID = 'I' and yearid = 2017 and est_gross_transaction_amt>0)
and csn not in (select csn from #inside)
and csn not in (select csn from #outside);

-- B in 2017, x in 2018
select count(distinct csn)
from link.view_facttrans_plus
  where csn not in (select csn from #inside) and csn not in (select csn from #outside)
  and csn in (select csn from link.view_facttrans_plus where corporation_id not in ('FP','101770000000','100020000000','101780000000','OCBC')
  and pool_id='P01' and club_code='LNK' and cancel_ind='N' and pointid='I' and est_gross_transaction_amt>0 and yearid=2017 and monthid::int<=12) 
  and csn in (select csn from link.view_facttrans_plus where corporation_id in ('FP','101770000000','100020000000','101780000000','OCBC')
  and pool_id='P01' and club_code='LNK' and cancel_ind='N' and pointid='I' and est_gross_transaction_amt>0 and yearid=2017 and monthid::int<=12);

-- C in 2017, x in 2018
select count(distinct csn)
FROM link.view_facttrans_plus
WHERE corporation_id  not IN ('FP','101770000000','100020000000','101780000000','OCBC')
AND POOL_ID='P01' AND CLUB_CODE ='LNK' AND CANCEL_IND = 'N' and pointID = 'I' and yearid = 2017 and est_gross_transaction_amt>0
and csn not in (select csn from link.view_facttrans_plus
WHERE corporation_id IN ('FP','101770000000','100020000000','101780000000','OCBC')
AND POOL_ID='P01' AND CLUB_CODE ='LNK' AND CANCEL_IND = 'N' and pointID = 'I' and yearid = 2017 and est_gross_transaction_amt>0)
and csn not in (select csn from #inside)
and csn not in (select csn from #outside);

-- Check B customer count in 2018
select count(distinct csn)
from link.view_facttrans_plus
  where csn in (select csn from #inside) and csn in (select csn from #outside);
  
-- B in 2017
select count(distinct csn)
from link.view_facttrans_plus
  where csn in (select csn from link.view_facttrans_plus where corporation_id not in ('FP','101770000000','100020000000','101780000000','OCBC')
  and pool_id='P01' and club_code='LNK' and cancel_ind='N' and pointid='I' and est_gross_transaction_amt>0 and yearid=2017 and monthid::int<=12) 
  and csn in (select csn from link.view_facttrans_plus where corporation_id in ('FP','101770000000','100020000000','101780000000','OCBC')
  and pool_id='P01' and club_code='LNK' and cancel_ind='N' and pointid='I' and est_gross_transaction_amt>0 and yearid=2017 and monthid::int<=12);

-- monthly count of B
select yearid, monthid, count(distinct csn) from #both
group by yearid, monthid;
-- cumulative
select count(distinct csn) from #inside
where csn in (select csn from #outside where monthid::int<=12) and monthid::int<=12;

-- Check next month ABC status [B] 
select count(distinct csn),
case 
  when csn in (select csn from #inside where monthid='02')
  and csn not in (select csn from #outside where monthid='02') then 'A'
  when csn in (select csn from #inside where monthid='02')
  and csn in (select csn from #outside where monthid='02') then 'B'
  when csn not in (select csn from #inside where monthid='02')
  and csn in (select csn from #outside where monthid='02') then 'C'
  else 'churn' end as abc
from link.view_facttrans_plus
  where csn in (select csn from #both where monthid='01')
 group by abc;
  
-- Check next month ABC status [A]
select count(distinct csn),
case 
  when csn in (select csn from #inside where monthid='02')
  and csn not in (select csn from #outside where monthid='02') then 'A'
  when csn in (select csn from #both where monthid='02')
  and csn in (select csn from #outside where monthid='02') then 'B'
  when csn not in (select csn from #inside where monthid='02')
  and csn in (select csn from #outside where monthid='02') then 'C'
  else 'churn' end as abc
from link.view_facttrans_plus
  where csn in (select csn from #inside where monthid='01') 
  and csn not in (select csn from #outside where monthid='01')
 group by abc;
 
-- Check next month ABC status [new to B] 
select count(distinct csn),
case 
  when csn in (select csn from #inside where monthid='12')
  and csn not in (select csn from #outside where monthid='12') then 'A'
  when csn in (select csn from #inside where monthid='12')
  and csn in (select csn from #outside where monthid='12') then 'B'
  when csn not in (select csn from #inside where monthid='12')
  and csn in (select csn from #outside where monthid='12') then 'C'
  else 'churn' end as abc
from link.view_facttrans_plus
where 
  csn in (select csn from #both where monthid='11') 
  and csn not in (select csn from #both where monthid::int<11)
group by abc;

-- Check next month spend [A]
select avg(spend) from
(select csn, sum(est_gross_transaction_amt) as spend from link.view_facttrans_plus 
  where pool_id='P01' and club_code='LNK' and cancel_ind='N' and pointid='I' and est_gross_transaction_amt>0 and yearid=2018 and monthid='02' 
  and csn in (select csn from #inside where monthid='01') 
  and csn not in (select csn from #outside where monthid='01')
 group by csn);

-- Check next month spend [B]
select avg(spend) from
(select csn, sum(est_gross_transaction_amt) as spend from link.view_facttrans_plus 
  where pool_id='P01' and club_code='LNK' and cancel_ind='N' and pointid='I' and est_gross_transaction_amt>0 and yearid=2018 and monthid='02' 
  and csn in (select csn from #inside where monthid='01') 
  and csn in (select csn from #outside where monthid='01')
 group by csn);
 
-- Check next month spend [new to B]
select avg(spend) from
(select csn, sum(est_gross_transaction_amt) as spend from link.view_facttrans_plus 
  where pool_id='P01' and club_code='LNK' and cancel_ind='N' and pointid='I' and est_gross_transaction_amt>0 and yearid=2018 and monthid='12' 
  and csn in (select csn from #both where monthid='11') 
  and csn not in (select csn from #both where monthid::int<11)
 group by csn);

-- Check next month spend [C]
select avg(spend) from
(select csn, sum(est_gross_transaction_amt) as spend from link.view_facttrans_plus 
  where pool_id='P01' and club_code='LNK' and cancel_ind='N' and pointid='I' and est_gross_transaction_amt>0 and yearid=2018 and monthid='02' 
  and csn not in (select csn from #inside where monthid='01') 
  and csn in (select csn from #outside where monthid='01')
 group by csn);
 
-- New to B: previous month status
select count(distinct csn),
case 
  when csn in (select csn from #inside where monthid='03')
  and csn not in (select csn from #outside where monthid='03') then 'A'
  when csn in (select csn from #both where monthid='03')
  and csn in (select csn from #outside where monthid='03') then 'B'
  when csn not in (select csn from #inside where monthid='03')
  and csn in (select csn from #outside where monthid='03') then 'C'
  else 'churn' end as abc
from link.view_facttrans_plus
  where 
  csn in (select csn from #both where monthid='04') 
  and csn not in (select csn from #both where monthid::int<04)
 group by abc;

-- Distribution in number of months in B
select csn, min(monthid) as Activation, count(monthid) as Active
into #numberofmonths
from #both
group by csn;

select active, count(csn) from #numberofmonths where activation='01'
group by active
order by active;

-- Change in spend
select avg(spend) from
(select csn, sum(est_gross_transaction_amt) as spend from link.view_facttrans_plus 
  where pool_id='P01' and club_code='LNK' and cancel_ind='N' and pointid='I' and est_gross_transaction_amt>0 and yearid=2018 and monthid='02' 
  and csn in (select csn from #iboth where monthid='02') 
  and csn not in (select csn from #both where monthid::int<04)
 group by csn)

-- Merchants
select a.corporation_id, corporate_name_english_1 as Merchant, count(distinct a.csn) as ActivatedCustomers from link.view_facttrans_plus a join #numberofmonths b
on a.csn=b.csn and a.monthid=b.Activation 
join link.link_cls_corporation c on a.corporation_id=c.corporate_id
where pool_id='P01' and club_code='LNK' and cancel_ind='N' and pointid='I' and est_gross_transaction_amt>0
and corporation_id not in ('FP','101770000000','100020000000','101780000000','OCBC')
and b.Activation::int>06
group by a.corporation_id, Merchant
order by ActivatedCustomers desc;

-- New to B: attributable to campaign?
drop table if exists #new07;
select distinct csn, monthid
into #new07
from link.view_facttrans_plus
  where 
  csn in (select csn from #both where monthid='07') 
  and csn not in (select csn from #both where monthid::int<07)
  and monthid='07';
-- 
select csn from #new07
where csn not in
(select csn from link.link_fact_cmp_campaign_email_stats join link.link_cmp_promotion on promotion_id=promotion_id_pk where open_ind=1
and promotion_date between '2018-06-01' and '2018-07-30')
and
csn not in
(select csn from lac.kp_v2_cat_rnk where deal_start_date<'2018-08-01');

--------------------------------------------------------------------------------------- Are B customers more loyal? -------------------------------------------------------------------------------------------

-- Do B customers have higher FP spending than A customers?
drop table if exists #FPspend;
select a.csn, a.abc, sum(b.est_gross_transaction_amt) as FPSpend, sum(b.transcount) as FPTransactions into #FPspend from #2018ABC a left join link.view_facttrans_plus b on a.csn=b.csn
where corporation_id in ('FP') and pool_id='P01' and club_code='LNK' and cancel_ind='N' and pointid='I' 
and transaction_date between '2018-01-01' and '2018-12-31' and est_gross_transaction_amt>0
group by a.csn, a.abc;

select abc, sum(FPSpend)/count(csn) as AvgSpend, sum(FPTransactions)/count(csn) as AvgTransCount from #FPspend
group by abc;

-- Do B customers have higher internal spending than A customers?
drop table if exists #internalspend;
select a.csn, a.abc, sum(b.est_gross_transaction_amt) as InternalSpend, sum(b.transcount) as InternalTransactions into #internalspend from #2018ABC a left join link.view_facttrans_plus b on a.csn=b.csn
where corporation_id in ('FP','101770000000','100020000000','101780000000','OCBC') and pool_id='P01' and club_code='LNK' and cancel_ind='N' and pointid='I' 
and transaction_date between '2018-01-01' and '2018-12-31' and est_gross_transaction_amt>0
group by a.csn, a.abc;

select abc, sum(InternalSpend)/count(csn) as AvgSpend, sum(InternalTransactions)/count(csn) as AvgTransCount from #internalspend
group by abc;

-- For ANOVA
select csn, abc, InternalSpend, InternalTransactions from #internalspend;

-- Do B customers have higher external spending than C customers?
drop table if exists #externalspend;
select a.csn, a.abc, sum(b.est_gross_transaction_amt) as ExternalSpend, sum(b.transcount) as ExternalTransactions into #externalspend from #2018ABC a left join link.view_facttrans_plus b on a.csn=b.csn
where corporation_id not in ('FP','101770000000','100020000000','101780000000','OCBC') and pool_id='P01' and club_code='LNK' and cancel_ind='N' and pointid='I' 
and transaction_date between '2018-01-01' and '2018-12-31' and est_gross_transaction_amt>0
group by a.csn, a.abc;

select abc, sum(ExternalSpend)/count(csn) as AvgSpend, sum(ExternalTransactions)/count(csn) as AvgTransCount from #externalspend
group by abc;

-- For ANOVA
select csn, abc, ExternalSpend, ExternalTransactions from #externalspend;

----------------------------------------------------------------------------- Is there a relationship between # merchants and engagement? ------------------------------------------------------------------------
drop table if exists #UniqueMerchants;
select a.csn, a.abc, count (distinct corporation_id) as UniqueMerchants, sum(b.est_gross_transaction_amt) as Spend, sum(transcount) as Transactions into #UniqueMerchants from #2018ABC a left join link.view_facttrans_plus b on a.csn=b.csn
where pool_id='P01' and club_code='LNK' and cancel_ind='N' and pointid='I' 
and transaction_date between '2018-01-01' and '2018-12-31' and est_gross_transaction_amt>0
group by a.csn, a.abc;

select UniqueMerchants, count(csn) as CustomerCount, sum(spend) as TotalSpend, sum(spend)/count(csn) as SpendPerCustomer, sum(Transactions)/count(csn) as AvgTransactionCount from #UniqueMerchants
group by UniqueMerchants
order by UniqueMerchants;

-- only for B
drop table if exists #BUniqueMerchants;
select csn, count (distinct corporation_id) as UniqueMerchants, sum(est_gross_transaction_amt) as Spend, sum(transcount) as Transactions into #BUniqueMerchants from link.view_facttrans_plus
where pool_id='P01' and club_code='LNK' and cancel_ind='N' and pointid='I' 
and transaction_date between '2018-01-01' and '2018-12-31' and est_gross_transaction_amt>0
and csn in (select csn from #inside)
and csn in (select csn from #outside)
group by csn;

select a.csn, UniqueMerchants, Spend, Transactions, count(distinct campaign_email_stats_id_pk) as NumberofOpens
from #BuniqueMerchants a left join link.link_fact_cmp_campaign_email_stats b on a.csn=b.csn 
where b.open_ind='1'
group by a.csn, UniqueMerchants, Spend, Transactions;


select UniqueMerchants, count(csn) as CustomerCount, sum(spend) as TotalSpend, sum(spend)/count(csn) as SpendPerCustomer, sum(Transactions)/count(csn) as AvgTransactionCount from #BUniqueMerchants
group by UniqueMerchants
order by UniqueMerchants;

select avg(UniqueMerchants), count(csn) as CustomerCount, sum(spend) as TotalSpend, sum(spend)/count(csn) as SpendPerCustomer, sum(Transactions)/count(csn) as AvgTransactionCount from #BUniqueMerchants;

---------------------------------------------------------------------------------------- Does contactability affect ABC status? -----------------------------------------------------------------------------------
drop table if exists #Contactability;
select a.csn, abc,
case when
  (comms_by_sms_flag='Y' and valid_sms='Y') or (comms_by_email_flag='Y' and valid_email='Y' and hard_bounce='N') then '1'
  else '0' end as Contactable
into #Contactability
from #2018ABC a join link.link_client_master b on a.csn=b.csn;

select Contactable, abc, count(csn) from #Contactability
group by Contactable, abc
order by Contactable, abc;

drop table if exists #EDMContactability;
select a.csn, abc,
case when
  (comms_by_email_flag='Y' and valid_email='Y' and hard_bounce='N') then '1'
  else '0' end as EDM
into #EDMContactability
from #2018ABC a join link.link_client_master b on a.csn=b.csn;

select EDM, abc, count(csn) from #EDMContactability
group by EDM, abc
order by EDM, abc;

-- Number of emails opened
drop table if exists #EDMopens;
select a.csn, a.abc, count (distinct campaign_email_stats_id_pk) as NumberofOpens
into #EDMopens
from #EDMContactability a left join link.link_fact_cmp_campaign_email_stats b on a.csn=b.csn 
where b.open_ind='1'
group by a.csn, a.abc;

drop table if exists #EDMsent;
select a.csn, a.abc, count (distinct campaign_email_stats_id_pk) as NumberofEDM
into #EDMsent
from #EDMContactability a left join link.link_fact_cmp_campaign_email_stats b on a.csn=b.csn 
group by a.csn, a.abc;


-- final table
csn abc fpspend internalspend internaltransactions externalspend externaltransactions uniquemerchants binary(courts,metro,mothercare,caltex) totalspend transactioncount contactability edmsent edmopened 



