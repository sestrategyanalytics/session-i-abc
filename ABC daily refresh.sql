drop table if exists lac.csn_abc;

select distinct csn 
into #1year_A from link.view_facttrans_plus
where corporation_id in ('FP','101770000000','100020000000','101780000000','OCBC')
and pool_id='P01' and club_code='LNK' and cancel_ind='N' and pointid='I' and transaction_date>=dateadd(month, -12, getdate()) and est_gross_transaction_amt>0
and csn not in
(select distinct csn from link.view_facttrans_plus
where corporation_id not in ('FP','101770000000','100020000000','101780000000','OCBC')
and pool_id='P01' and club_code='LNK' and cancel_ind='N' and pointid='I' and transaction_date>=dateadd(month, -12, getdate()) and est_gross_transaction_amt>0);

select distinct csn 
into #1year_B from link.view_facttrans_plus
where corporation_id in ('FP','101770000000','100020000000','101780000000','OCBC')
and pool_id='P01' and club_code='LNK' and cancel_ind='N' and pointid='I' and transaction_date>=dateadd(month, -12, getdate()) and est_gross_transaction_amt>0
and csn in
(select distinct csn from link.view_facttrans_plus
where corporation_id not in ('FP','101770000000','100020000000','101780000000','OCBC')
and pool_id='P01' and club_code='LNK' and cancel_ind='N' and pointid='I' and transaction_date>=dateadd(month, -12, getdate()) and est_gross_transaction_amt>0);

select distinct csn 
into #1year_C from link.view_facttrans_plus
where corporation_id not in ('FP','101770000000','100020000000','101780000000','OCBC')
and pool_id='P01' and club_code='LNK' and cancel_ind='N' and pointid='I' and transaction_date>=dateadd(month, -12, getdate()) and est_gross_transaction_amt>0
and csn not in
(select distinct csn from link.view_facttrans_plus
where corporation_id in ('FP','101770000000','100020000000','101780000000','OCBC')
and pool_id='P01' and club_code='LNK' and cancel_ind='N' and pointid='I' and transaction_date>=dateadd(month, -12, getdate()) and est_gross_transaction_amt>0);

select distinct csn 
into #90d_A from link.view_facttrans_plus
where corporation_id in ('FP','101770000000','100020000000','101780000000','OCBC')
and pool_id='P01' and club_code='LNK' and cancel_ind='N' and pointid='I' and transaction_date>=dateadd(month, -3, getdate()) and est_gross_transaction_amt>0
and csn not in
(select distinct csn from link.view_facttrans_plus
where corporation_id not in ('FP','101770000000','100020000000','101780000000','OCBC')
and pool_id='P01' and club_code='LNK' and cancel_ind='N' and pointid='I' and transaction_date>=dateadd(month, -3, getdate()) and est_gross_transaction_amt>0);

select distinct csn 
into #90d_B from link.view_facttrans_plus
where corporation_id in ('FP','101770000000','100020000000','101780000000','OCBC')
and pool_id='P01' and club_code='LNK' and cancel_ind='N' and pointid='I' and transaction_date>=dateadd(month, -3, getdate()) and est_gross_transaction_amt>0
and csn in
(select distinct csn from link.view_facttrans_plus
where corporation_id not in ('FP','101770000000','100020000000','101780000000','OCBC')
and pool_id='P01' and club_code='LNK' and cancel_ind='N' and pointid='I' and transaction_date>=dateadd(month, -3, getdate()) and est_gross_transaction_amt>0);

select distinct csn 
into #90d_C from link.view_facttrans_plus
where corporation_id not in ('FP','101770000000','100020000000','101780000000','OCBC')
and pool_id='P01' and club_code='LNK' and cancel_ind='N' and pointid='I' and transaction_date>=dateadd(month, -3, getdate()) and est_gross_transaction_amt>0
and csn not in
(select distinct csn from link.view_facttrans_plus
where corporation_id in ('FP','101770000000','100020000000','101780000000','OCBC')
and pool_id='P01' and club_code='LNK' and cancel_ind='N' and pointid='I' and transaction_date>=dateadd(month, -3, getdate()) and est_gross_transaction_amt>0);

select distinct csn 
into #30d_A from link.view_facttrans_plus
where corporation_id in ('FP','101770000000','100020000000','101780000000','OCBC')
and pool_id='P01' and club_code='LNK' and cancel_ind='N' and pointid='I' and transaction_date>=dateadd(month, -1, getdate()) and est_gross_transaction_amt>0
and csn not in
(select distinct csn from link.view_facttrans_plus
where corporation_id not in ('FP','101770000000','100020000000','101780000000','OCBC')
and pool_id='P01' and club_code='LNK' and cancel_ind='N' and pointid='I' and transaction_date>=dateadd(month, -1, getdate()) and est_gross_transaction_amt>0);

select distinct csn 
into #30d_B from link.view_facttrans_plus
where corporation_id in ('FP','101770000000','100020000000','101780000000','OCBC')
and pool_id='P01' and club_code='LNK' and cancel_ind='N' and pointid='I' and transaction_date>=dateadd(month, -1, getdate()) and est_gross_transaction_amt>0
and csn in
(select distinct csn from link.view_facttrans_plus
where corporation_id not in ('FP','101770000000','100020000000','101780000000','OCBC')
and pool_id='P01' and club_code='LNK' and cancel_ind='N' and pointid='I' and transaction_date>=dateadd(month, -1, getdate()) and est_gross_transaction_amt>0);

select distinct csn 
into #30d_C from link.view_facttrans_plus
where corporation_id not in ('FP','101770000000','100020000000','101780000000','OCBC')
and pool_id='P01' and club_code='LNK' and cancel_ind='N' and pointid='I' and transaction_date>=dateadd(month, -1, getdate()) and est_gross_transaction_amt>0
and csn not in
(select distinct csn from link.view_facttrans_plus
where corporation_id in ('FP','101770000000','100020000000','101780000000','OCBC')
and pool_id='P01' and club_code='LNK' and cancel_ind='N' and pointid='I' and transaction_date>=dateadd(month, -1, getdate()) and est_gross_transaction_amt>0);

select csn, case when csn in (select csn from #30d_A) then 'A' when csn in (select csn from #30d_B) then 'B' when csn in (select csn from #30d_C) then 'C' else 'I' end as thirtydays,
case when csn in (select csn from #90d_A) then 'A' when csn in (select csn from #90d_B) then 'B' when csn in (select csn from #90d_C) then 'C' else 'I' end as ninetydays,
case when csn in (select csn from #1year_A) then 'A' when csn in (select csn from #1year_B) then 'B' when csn in (select csn from #1year_C) then 'C' else 'I' end as oneyear
into lac.csn_abc
from lac.csn_cltv;

grant select on lac.csn_abc to group lacusers;
