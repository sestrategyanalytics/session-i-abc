# Session I: ABC

## Context
ABC is a business strategy by NTUC Link to diversify the spend of members in the Plus! program. 
Currently, a significant proportion of our revenue comes from FairPrice and OCBC. 
While we will continue to be a FairPrice-centric loyalty program for the foreseeable future, we want to build on this foundation while growing our other partners.

'B' members have been shown to be more valuable (both to Link and FairPrice) and less likely to become inactive [see 'ABC A Statistical Analysis.ppt' for detailed analysis],
so converting members to 'B' should be a key objective in our Customer Life Cycle management strategy.

In 2019, ABC remains a key angle in our transformation journey, and there is still a lot of untapped potential.

## Definitions
Inside spend: 

- NTUC FairPrice (including Unity and Cheers)
- corporation_id in ('FP','101770000000','100020000000','101780000000')

Oustide spend:

- All other merchant partners excluding OCBC
- corporation_id not in ('FP','101770000000','100020000000','101780000000','OCBC')

OCBC spend:

- OCBC
- corporation_id = 'OCBC'


| Group | Definition |
|----------|-------------|
| A | Only inside spend |
| B | Both inside and outside spend |
| C | Only outside spend and OCBC-only spend |


## History
In 2018, we set a target of reaching 1,000,000 'B' members for the year. We fell short, at 718k. 
The biggest area of concern is that over 300k of them exhibited 'B' behaviour in fewer than 3 of the 12 months [See 'ABC A Statistical Analysis.ppt' for detailed analysis].

EDM Campaigns have had limited effectiveness [See 'Campaign Analysis' for details] in shaping customer behaviour.

This year, we have shifted our focus from 'acquisition' to 'retention'.


## Future Scope
LAC has generated many insights which have yet to be acted on, due to limitations in Link's capabilities. Our suggestions include:

- Real-time communications and fulfilment
- Content marketing
- Personalized offers and comms
- Gamification
