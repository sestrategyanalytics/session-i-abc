"""
ABC Daily Refresh
"""
from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from airflow.operators.email_operator import EmailOperator
from datetime import datetime, timedelta
from time import strftime, gmtime
from dateutil import relativedelta

default_args = {
    'owner': 'manfred',
    'depends_on_past': False,
    'start_date': datetime(2019, 4, 16),
    'email': ['manfredng@ntuclink.com.sg'],
    'email_on_failure': True,
    'email_on_retry': True,
    'retries': 1,
    'retry_delay': timedelta(minutes=1),
    #'schedule_interval':'@weekly', # run on every sunday 8pm
    #'schedule_interval':'0 12 * * 0 *', # run on every sunday 8pm
    # 'queue': 'bash_queue',
    # 'pool': 'backfill',
    # 'priority_weight': 10,
    # 'end_date': datetime(2016, 1, 1),
    }

# DAG
dag = DAG(
    'manfred_daily_abc',
    default_args=default_args,
    schedule_interval = '0 23 * * *',    # daily at 7am
    dagrun_timeout=timedelta(minutes=30)
    )

params = {
    'home_dir':'/home/manfred'
}

# tasks

test_task = BashOperator(
    task_id='manfred_create_abc_table',
    bash_command="""
    cd {{ params.home_dir }};
    python manfred_daily_abc.py
    """,
    params=params,
    dag=dag)

send_email = EmailOperator(
    to= ['manfredng@ntuclink.com.sg'],
    task_id='email_task',
    subject='{{ dag }}: start_date {{ ds }}',
    params={'content1': 'random'},
    html_content="""
    task_key - {{ task_instance_key_str }} <br/>
    test_mode - {{ test_mode }} <br/>
    task_owner - {{ task.owner}} <br/>
    hostname - {{ ti.hostname }}
    """,
    dag = dag)

send_email.set_upstream(test_task)
